#include <sys/types.h>	
#include <sys/socket.h>	
#include <arpa/inet.h>
#include <netinet/in.h>	
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string>
#include <thread>
#include <vector>
#include <iostream>
#include <string.h>

#define PORT_NUMBER 8130
#define MAXLINE 200
#define LISTENQ 5

using namespace std;

struct File{
    char file_name[40];
    char file_path[MAXLINE];
};

class Client {
public:
	Client() {
		isCache = false;
	}
	bool isCache;
	char IP[40];
	char Port[40];
	vector<Client> cached_clients;
	vector<File> files_shared;
};

vector<Client> clients;
int NUMCACHED = 0;

void n_interactive();
void parse(vector<char *> &cmd, char *input);
void sign_cmd(int connfd, char * IPtoPass, char * buff);
void gbye_cmd(int connfd, char * IPtoPass);
void cach_cmd(int connfd);
void list_cmd(int connfd);
void make_cache(vector<char *> &cmd);
void Shutdown();

int main(int argc, char **argv)
{
    printf("*** Server now active. ***\n");
    printf("Type \"shutdown\" to close all connections.\n");
    thread ni_thread(n_interactive);

    for ( ; ; ) {// infinite loop to accept then serve conncection
    	
	    char input[MAXLINE];
	    printf("$ ");
	    fgets(input, MAXLINE, stdin);
        vector<char *> cmd;
	    parse(cmd, input);
    	if (strcmp(cmd[0], "list\n") == 0){
	      printf("*** Listing registered clients ***\n");
	      if (clients.size() == 0) cout << "There are no registered clients." << endl;
	      else{
		    for (int i = 0; i < clients.size(); i++){
		      if (clients[i].isCache)
		        printf("CACHED");
		      cout << "\tclient\t" << clients[i].IP << "\t" << clients[i].Port << endl;
		      if (clients[i].isCache){
		      	cout << '\t' << "This cache-server tracks the following clients:" << endl;
		      	for (int j = 0; j < clients[i].cached_clients.size(); j++){
		      		cout << "\t\t" << "client" << '\t' << clients[i].cached_clients[j].IP << '\t' << clients[i].cached_clients[j].Port << endl;
		      	}
		      }
		    }
	  	  }
	    }
	    else if (strcmp(cmd[0], "cache") == 0){
	    	make_cache(cmd);
	    }
	    else if (strcmp(cmd[0], "shutdown\n") == 0){
	    	// loop through clients, write to them to shutdown
	    	Shutdown();
	      exit(0);
	    }
    }
    ni_thread.join();
}

void parse(vector<char *> &cmd, char *input){
    char * token;
    token = strtok(input, " ");
    cmd.push_back(token);
    while(token != NULL){
    	token = strtok(NULL, " ");
        cmd.push_back(token);
    }
}

void Shutdown(){
	printf("*** Beginning shutdown process ***\n");
	int sockfd, n, msg, len;
	char recvline[MAXLINE + 1];
    struct sockaddr_in  cliaddr;

    for (int i = 0; i < clients.size(); i++){
    	if (clients[i].isCache == true){
    		for (int j = 0; j < clients[i].cached_clients.size(); j++){
    			cout << "Attempting to shutdown client with IP " << clients[i].cached_clients[j].IP << " and port " << clients[i].cached_clients[j].Port << endl;
		    	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
			        return;
			    }
			    bzero(&cliaddr, sizeof(cliaddr));
			    cliaddr.sin_family = AF_INET;
			    unsigned short port = (unsigned short) strtoul(clients[i].cached_clients[j].Port, NULL, 0);
			    cliaddr.sin_port   = htons(port);

			    if (inet_pton(AF_INET, clients[i].cached_clients[j].IP, &cliaddr.sin_addr) <= 0) {
			        fprintf( stderr, "inet_pton error for %s\n", clients[i].cached_clients[j].IP );
			        return;
			    }

			    // Attempt to connect to the server.
			    if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
			        fprintf( stderr, "connect error: %s\n", strerror( errno ) );
			        return;
			    }
			    printf("\t*** Successful connection to client. ***\n");

			    char cmd[] = "shut";
			    if ((write(sockfd,cmd,sizeof(cmd))) < 0) {
			        printf("client: write error when sending \"shut\" command  :%s\n",  strerror( errno )); 
			        return;
			    }
			    printf("\t*** \"shut\" command successfully sent to server ***.\n");

				if ((len = read(sockfd, recvline, MAXLINE)) < 0){
					fprintf(stderr, "client: read error after sending \"shut\" command :%s\n", strerror(errno));
					return;
				}
				recvline[len] = '\0';

				if (len < 0) {
					printf("%i\n", n);
					fprintf( stderr, "read error after sending \"shut\" command : %s\n", strerror( errno ) );
					return;
				}
				cout << "Client successfully shutdown" << endl;
				close( sockfd );
				cout << endl;
    		}

    		cout << "Attempting to shutdown client with IP " << clients[i].IP << " and port " << clients[i].Port << endl;
	    	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
		        return;
		    }
		    bzero(&cliaddr, sizeof(cliaddr));
		    cliaddr.sin_family = AF_INET;
		    unsigned short port = (unsigned short) strtoul(clients[i].Port, NULL, 0);
		    cliaddr.sin_port   = htons(port);

		    if (inet_pton(AF_INET, clients[i].IP, &cliaddr.sin_addr) <= 0) {
		        fprintf( stderr, "inet_pton error for %s\n", clients[i].IP );
		        return;
		    }

		    // Attempt to connect to the server.
		    if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
		        fprintf( stderr, "connect error: %s\n", strerror( errno ) );
		        return;
		    }
		    printf("\t*** Successful connection to client. ***\n");

		    char cmd[] = "shut";
		    if ((write(sockfd,cmd,sizeof(cmd))) < 0) {
		        printf("client: write error when sending \"shut\" command  :%s\n",  strerror( errno )); 
		        return;
		    }
		    printf("\t*** \"shut\" command successfully sent to server ***.\n");

			if ((len = read(sockfd, recvline, MAXLINE)) < 0){
				fprintf(stderr, "client: read error after sending \"shut\" command :%s\n", strerror(errno));
				return;
			}
			recvline[len] = '\0';

			if (len < 0) {
				printf("%i\n", n);
				fprintf( stderr, "read error after sending \"shut\" command : %s\n", strerror( errno ) );
				return;
			}
			cout << "Client successfully shutdown" << endl;
			close( sockfd );
			cout << endl;
    	}
    	else{
	    	cout << "Attempting to shutdown client with IP " << clients[i].IP << " and port " << clients[i].Port << endl;
	    	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
		        return;
		    }
		    bzero(&cliaddr, sizeof(cliaddr));
		    cliaddr.sin_family = AF_INET;
		    unsigned short port = (unsigned short) strtoul(clients[i].Port, NULL, 0);
		    cliaddr.sin_port   = htons(port);

		    if (inet_pton(AF_INET, clients[i].IP, &cliaddr.sin_addr) <= 0) {
		        fprintf( stderr, "inet_pton error for %s\n", clients[i].IP );
		        return;
		    }

		    // Attempt to connect to the server.
		    if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
		        fprintf( stderr, "connect error: %s\n", strerror( errno ) );
		        return;
		    }
		    printf("\t*** Successful connection to client. ***\n");

		    char cmd[] = "shut";
		    if ((write(sockfd,cmd,sizeof(cmd))) < 0) {
		        printf("client: write error when sending \"shut\" command  :%s\n",  strerror( errno )); 
		        return;
		    }
		    printf("\t*** \"shut\" command successfully sent to server ***.\n");

			if ((len = read(sockfd, recvline, MAXLINE)) < 0){
				fprintf(stderr, "client: read error after sending \"shut\" command :%s\n", strerror(errno));
				return;
			}
			recvline[len] = '\0';

			if (len < 0) {
				printf("%i\n", n);
				fprintf( stderr, "read error after sending \"shut\" command : %s\n", strerror( errno ) );
				return;
			}
			cout << "Client successfully shutdown" << endl;
			close( sockfd );
			cout << endl;
	    }
	}
	cout << "*** Ending shutdown process ***" << endl;
    cout << "*** Server now inactive ***" << endl;
}

void make_cache(vector<char *> &cmd){
	printf("Attempting to make client a cache-server\n");
	int sockfd, n, msg, len;
	char recvline[MAXLINE + 1];
    struct sockaddr_in  cliaddr;
    int cache_count = 0;
	for (int i = 0; i < clients.size(); i++){
		cache_count++;
	    if (cmd[2][strlen(cmd[2]) - 1] == '\n'){
	  	  cmd[2][strlen(cmd[2]) - 1] = '\0';
	  	}

		if (strcmp(clients[i].IP, cmd[1]) == 0 && strcmp(clients[i].Port, cmd[2]) == 0){
			cout << "*** Making client with IP " << clients[i].IP << " and port " << clients[i].Port << " a cache_server ***" << endl;

		    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
		        exit( 2 );
		    }
		    // build a profile for the server to whom we are going to
		    // communicate.  

		    bzero(&cliaddr, sizeof(cliaddr));
		    cliaddr.sin_family = AF_INET;
		    unsigned short port = (unsigned short) strtoul(clients[i].Port, NULL, 0);
		    cliaddr.sin_port   = htons(port);

		    if (inet_pton(AF_INET, cmd[1], &cliaddr.sin_addr) <= 0) {
		        fprintf( stderr, "inet_pton error for %s\n", cmd[1] );
		        exit( 3 );
		    }

		    // Attempt to connect to the server.
		    if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
		        fprintf( stderr, "connect error: %s\n", strerror( errno ) );
		        exit( 4 );
		    }
		    printf("*** Successful connection to client. ***\n");

		    char cmd[] = "serv";
		    if ((write(sockfd,cmd,sizeof(cmd))) < 0) {
		        printf("client: write error when sending \"make\" command :%s\n",  strerror( errno )); 
		        exit(0);
		    }
		    printf("*** \"cach\" command successfully sent to client ***.\n");

			if ((len = read(sockfd, recvline, MAXLINE)) < 0){
				fprintf(stderr, "client: read error after sending \"make\" command :%s\n", strerror(errno));
				exit(0);
			}
			recvline[len] = '\0';

			if (len < 0) {
				printf("%i\n", n);
				fprintf( stderr, "read error after sending \"make\" command : %s\n", strerror( errno ) );
				exit( 6 );
			}
			clients[i].isCache = true;
			NUMCACHED++;
			cout << "Client is now a documented cache-server" << endl << endl;


			int num_to_cache;
			cout << "How many clients do you want to add to this new cache-server? ";
			cin >> num_to_cache;
		    if ((write(sockfd, &num_to_cache, sizeof(num_to_cache))) < 0) {
		        printf("client: write error when sending number of clients to cache :%s\n",  strerror( errno )); 
		        exit(0);
		    }
		    printf("\n*** Number of clients to cache successfully sent to server ***.\n");

			if ((len = read(sockfd, recvline, MAXLINE)) < 0){
				fprintf(stderr, "client: read error after sending \"make\" command :%s\n", strerror(errno));
				exit(0);
			}
			recvline[len] = '\0';

			if (len < 0) {
				printf("%i\n", n);
				fprintf( stderr, "read error after sending \"make\" command : %s\n", strerror( errno ) );
				exit( 6 );
			}
			cout << "*** Preparing to send clients ***" << endl << endl;

			for (int k = 0; k < num_to_cache; k++){
				cout << "Please enter " << num_to_cache << " clients each separated by a newline." << endl;
				cout << "Identify the client you want to cache by the IP and port number\nseparated by a space (\"IP port\"):" << endl; 
				char cur_IP[40]; char cur_port[40];
				cin >> cur_IP >> cur_port;
				for (int j = 0; j < clients.size(); j++){
					if (strcmp(cur_IP, clients[j].IP) == 0 && strcmp(cur_port, clients[j].Port) == 0){
						if ((write(sockfd, &clients[j], sizeof(clients[j]))) < 0) {
		        			printf("client: write error when sending client :%s\n",  strerror( errno )); 
		        			exit(0);
		    			}
		    			printf("*** A client was successfully sent to the cache-server ***.\n");
						if ((len = read(sockfd, recvline, MAXLINE)) < 0){
							fprintf(stderr, "client: read error after sending client to cache :%s\n", strerror(errno));
							exit(0);
						}
						recvline[len] = '\0';

						if (len < 0) {
							printf("%i\n", n);
							fprintf( stderr, "read error after sending client to cache : %s\n", strerror( errno ) );
							exit( 6 );
						}
						clients[i].cached_clients.push_back(clients[j]);
						clients.erase(clients.begin() + j);
						cout << "*** Client accepted and cached. ***" << endl;
					}
				}
			}
			close( sockfd );
		}
	}
}

void sign_cmd(int connfd, char * IPtoPass, char * buff){
	cout << "*** New client signing on ***" << endl;
	Client c;
	strcpy(c.IP, IPtoPass);
	char cont[] = "Accepted";
	
	if( write(connfd, cont, strlen(cont)) < 0 ) {
	  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	  exit( 1 );
	}
	cout << "*** Starting client sign on ***" << endl;

    if (read(connfd, buff, sizeof(buff)) < 0){
      fprintf(stderr, "Read failed. %s\n", strerror(errno));
      exit(1);
    }
    strncpy(c.Port, buff, 40);

    if( write(connfd, cont, strlen(cont)) < 0 ) {
	  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	  exit( 1 );
	}
	cout << "*** Grabbed client's port number ***" << endl;

	clients.push_back(c);
	cout << "*** Added client to list of clients ***" << endl;
}

void cach_cmd(int connfd){
	cout << "Attempting to send the number of cache-servers to client" << endl;
	if( write(connfd, &NUMCACHED, sizeof(NUMCACHED)) < 0 ) {
	  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	  exit( 1 );
	}
	cout << "*** Wrote number to cache-servers to client ***" << endl;

	char buff[MAXLINE];
	if (read(connfd, &buff, sizeof(buff)) < 0){
      fprintf(stderr, "Read failed. %s\n", strerror(errno));
      exit(1);
    }
    cout << "*** Ready to start sending cache-server data ***" << endl;;

	for (int i = 0; i < NUMCACHED; i++){
		for (int j = 0; j < clients.size(); j++){
			if (clients[j].isCache){
				cout << "*** Sending cache-server IP ***" << endl;
				if( write(connfd, &clients[j].IP, sizeof(clients[j].IP)) < 0 ) {
				  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
				  exit( 1 );
				}
				cout << "*** Sent cache-server's IP ***" << endl;
				if (read(connfd, &buff, sizeof(buff)) < 0){
				  fprintf(stderr, "Read failed. %s\n", strerror(errno));
				  exit(1);
				}
				cout << "*** Sending cache-server's port ***" << endl;;
				if( write(connfd, &clients[j].Port, sizeof(clients[j].Port)) < 0 ) {
				  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
				  exit( 1 );
				}
				cout << "*** Sent cache-server's port ***" << endl;
				if (read(connfd, &buff, sizeof(buff)) < 0){
				  fprintf(stderr, "Read failed. %s\n", strerror(errno));
				  exit(1);
				}
				cout << "*** cache-server's data completed sending ***" << endl;
			}
		}
	}
}

void list_cmd(int connfd){
	cout << "*** Beginning list command ***" << endl;
	int num_clients = clients.size();
	if( write(connfd, &num_clients, sizeof(num_clients)) < 0 ) {
	  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	  return;
	}
	cout << "Wrote number of clients to client" << endl;

	char buff[MAXLINE];
	if (read(connfd, &buff, sizeof(buff)) < 0){
	  fprintf(stderr, "Read failed. %s\n", strerror(errno));
	  return;
	}
	cout << "*** Preparing to send client data ***" << endl;

	for (int i = 0; i < num_clients; i++){
		if( write(connfd, &clients[i].IP, sizeof(clients[i].IP)) < 0 ) {
		  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
		  return;
		}
		cout << "Wrote a client's IP to the client" << endl;
	
		if (read(connfd, &buff, sizeof(buff)) < 0){
		  fprintf(stderr, "Read failed. %s\n", strerror(errno));
		  return;
		}
		cout << "*** Sending client's port ***" << endl;;

		if( write(connfd, &clients[i].Port, sizeof(clients[i].Port)) < 0 ) {
		  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
		  return;
		}
		cout << "Wrote a client's port number to the client" << endl;

		if (read(connfd, &buff, sizeof(buff)) < 0){
		  fprintf(stderr, "Read failed. %s\n", strerror(errno));
		  return;
		}
		cout << "*** Client received client data ***" << endl;;
	}
	cout << "*** Ending list command ***" << endl;
}

void gbye_cmd(int connfd, char * IPtoPass){
	printf("*** Client shutting down ***\n");

	char cont[] = "Accepted";
	if( write(connfd, cont, strlen(cont)) < 0 ) {
	  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	  return;
	}

	char cur_port[40];
	if (read(connfd, cur_port, sizeof(cur_port)) < 0){
      fprintf(stderr, "Read failed. %s\n", strerror(errno));
      return;
    }
    cout << "*** Accepted client's port ***" << endl;
	
    int sockfd, n, msg, len;
    char recvline[MAXLINE + 1];
    struct sockaddr_in  servaddr, cliaddr;

    printf("Preparing to connect to client.\n");

    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
        exit( 2 );
    }
    // build a profile for the server to whom we are going to
    // communicate.  
    bzero(&cliaddr, sizeof(cliaddr));
    servaddr.sin_family = AF_INET;
    unsigned short port = (unsigned short) strtoul(cur_port, NULL, 0);
    servaddr.sin_port   = htons(port);

    if (inet_pton(AF_INET, IPtoPass, &cliaddr.sin_addr) <= 0) {
      fprintf( stderr, "inet_pton error for %s\n", IPtoPass );
      return;
    }
    // Attempt to connect to the server.
    if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
      fprintf( stderr, "connect error: %s\n", strerror( errno ) );
      return;
    }
    printf("*** Successful connection to client. ***\n");

    char cmd[] = "cach";
    if( write(connfd, &cmd, strlen(cmd)) < 0 ) {
	  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	  return;
	}

    if ((len = read(sockfd, &recvline, sizeof(recvline))) < 0){
      fprintf(stderr, "client: read error when reading response :%s\n", strerror(errno));
      return;
    }
    recvline[len] = '\0';
    if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error when reading response : %s\n", strerror( errno ) );
      return;
    }
    cout << "*** Received cache status ***" << endl;
    close(sockfd);

    if (strcmp(recvline, "yes") == 0){
		printf("Preparing to connect to client.\n");

	    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
	        exit( 2 );
	    }
	    // build a profile for the server to whom we are going to
	    // communicate.  
	    bzero(&cliaddr, sizeof(cliaddr));
	    servaddr.sin_family = AF_INET;
	    servaddr.sin_port   = htons(port);

	    if (inet_pton(AF_INET, IPtoPass, &cliaddr.sin_addr) <= 0) {
	      fprintf( stderr, "inet_pton error for %s\n", IP );
	      return;
	    }
	    // Attempt to connect to the server.
	    if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
	      fprintf( stderr, "connect error: %s\n", strerror( errno ) );
	      return;
	    }
	    printf("*** Successful connection to client. ***\n");

	    char cmd[] = "dump";
	    if( write(connfd, &cmd, strlen(cmd)) < 0 ) {
		  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
		  return;
		}
		cout << "*** Successful \"dump\" command sent ***" << endl;

        int clients_cached;
	    if ((len = read(sockfd, &clients_cached, sizeof(clients_cached))) < 0){
	      fprintf(stderr, "client: read error when reading number of clients cached :%s\n", strerror(errno));
	      return;
	    }
	    if (len < 0) {
	      printf("%i\n", n);
	      fprintf( stderr, "read error when reading number of clients cached : %s\n", strerror( errno ) );
	      return;
	    }
	    cout << "*** Received number of clients cached by the cache-server ***" << endl;

		if( write(connfd, cont, strlen(cont)) < 0 ) {
		  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
		  return;
		}

		for (int k = 0; k < clients_cached; k++){
			Client a_client;
		    if ((len = read(sockfd, &a_client.IP, sizeof(a_client.IP))) < 0){
		      fprintf(stderr, "client: read error when reading client's IP :%s\n", strerror(errno));
		      return;
		    }
		    if (len < 0) {
		      printf("%i\n", n);
		      fprintf( stderr, "read error when reading client's IP : %s\n", strerror( errno ) );
		      return;
		    }
		    cout << "*** Received client's IP ***" << endl;

			if( write(connfd, cont, strlen(cont)) < 0 ) {
			  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
			  return;
			}

		    if ((len = read(sockfd, &a_client.Port, sizeof(a_client.Port))) < 0){
		      fprintf(stderr, "client: read error when reading client's port :%s\n", strerror(errno));
		      return;
		    }
		    if (len < 0) {
		      printf("%i\n", n);
		      fprintf( stderr, "read error when reading client's port : %s\n", strerror( errno ) );
		      return;
		    }
		    cout << "*** Received client's port ***" << endl;
			
			if( write(connfd, cont, strlen(cont)) < 0 ) {
			  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
			  return;
			}
			clients.push_back(a_client);
		}
		close(sockfd);
    }

	for (int i = 0; i < clients.size(); i++){
		if (strcmp(clients[i].IP, IPtoPass) == 0 && strcmp(clients[i].Port, cur_port) == 0){
			clients.erase(clients.begin() + i);
			cout << "*** Client removed from active client list ***" << endl;
			if( write(connfd, cont, strlen(cont)) < 0 ) {
				fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
				return;
			}
			break;
		}
	}	
}

void n_interactive(){
    int	listenfd, connfd;
    socklen_t   len;
    struct sockaddr_in  servaddr, cliaddr;
    char	buff[MAXLINE];
	
    if( ( listenfd = socket(AF_INET, SOCK_STREAM, 0) ) < 0 ) {
		fprintf( stderr, "socket failed.  %s\n", strerror( errno ) );
		exit( 1 );
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;             // Communicate using the Internet domain (AF_INET)
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   // Who should we accept connections from?         
    servaddr.sin_port        = htons(PORT_NUMBER);  // Which port should the server listen on?        

    if( bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
	fprintf( stderr, "Bind failed.  %s\n", strerror( errno ) );
	exit( 1 );
    }

    if( listen(listenfd, LISTENQ) < 0 ) {
	fprintf( stderr, "Listen failed.  %s\n", strerror( errno ) );
	exit( 1 );
    }

	for ( ; ; ) {
		len = sizeof(cliaddr);

		if( ( connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len) ) < 0 ) {
		    fprintf( stderr, "Accept failed.  %s\n", strerror( errno ) );
		    exit( 1 );
		}
			    	    
	    printf("\nconnection from %s, port %d\n",
		   inet_ntop(AF_INET, &cliaddr.sin_addr, buff, sizeof(buff)),
		   ntohs(cliaddr.sin_port));
	    
	    char IPtoPass[MAXLINE];
	    strcpy(IPtoPass, buff);

	    if (read(connfd, buff, sizeof(buff)) < 0){
	      fprintf(stderr, "Read failed. %s\n", strerror(errno));
	      exit(1);
	    }
	    
	    if (strcmp(buff, "gbye") == 0){
	    	thread gbye_thread(gbye_cmd, connfd, IPtoPass);
	    	gbye_thread.join();
	    }
	    else if (strcmp(buff, "sign") == 0){
	   	    thread sign_on_thread(sign_cmd, connfd, IPtoPass, buff);
	    	sign_on_thread.join();
	    }
	    else if (strcmp(buff, "cach") == 0){
	    	thread cach_thread(cach_cmd, connfd);
	    	cach_thread.join();
	    }
	    else if (strcmp(buff, "list") == 0){
	    	thread list_thread(list_cmd, connfd);
	    	list_thread.join();
	    }

	    close(connfd);
	}
    close(listenfd);
}