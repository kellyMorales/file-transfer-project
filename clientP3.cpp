#include <sys/types.h>	
#include <sys/socket.h>	/* basic socket definitions */
#include <netinet/in.h>	/* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <vector>
#include <thread>
#include <sys/stat.h>

#define MAXLINE 200
#define SERVER_PORT 8130
#define LISTENQ 5

using namespace std;

struct File{
    char file_name[MAXLINE];
    char file_path[MAXLINE];
};

class Client {
public:
    Client() {
        isCache = false;
    }
    bool isCache;
    char IP[40];
    char Port[40];
    vector<File> files_shared;

};

bool ISCACHE = false;
char MYPORT[40];
char IP[40];
vector<File> files_shared;
vector<Client> clients_if_cached;

void parse(vector<char *> &cmd, char *input);
void n_interactive();
void Register();
void List();
void Share(vector<char *> &path);
void Download(vector<char *> &cmd);
void Shutdown();
void shut_cmd(int connfd);
void list_cmd(int connfd);
void file_cmd(int connfd);
void cach_cmd(int connfd);
void dump_cmd(int connfd);
void serv_cmd(int connfd);
void ls_cmd(vector<char *> &cmd);
void parse_path(vector<char *> &cmd, vector<char*> &path);

int main(int argc, char **argv)
{
  if (argc != 2) {
    fprintf( stderr, "Usage: %s <IPaddress>\n", argv[0] );
    exit( 1 );
  }  
  strcpy(IP, argv[1]);
  cout << "Enter this client's port number: ";
  cin >> MYPORT;
  Register();
  thread ni_thread(n_interactive);
  for(;;) {

    char input[MAXLINE];
    printf("$ ");
    fgets(input, MAXLINE, stdin);
    vector<char *> cmd;
    parse(cmd, input);

    if (strcmp(cmd[0], "shutdown\n") == 0){
      Shutdown();
      exit(0);
    }
    else if (strcmp(cmd[0], "status\n") == 0){
      if (ISCACHE){
        printf("This client is a cache-server.\n");
      }
      else printf("This client is not a cache-server.\n");
      
      if (files_shared.size() == 0) printf("This client has shared no files.\n");
      else{
        printf("This client has shared the following files:\n");
        for (int i = 0; i < files_shared.size(); i++)
          cout << files_shared[i].file_name << '\t';
        cout << endl;
      }
    }
    else if (strcmp(cmd[0], "ls\n") == 0 || strcmp(cmd[0], "ls") == 0){
        ls_cmd(cmd);
    }
    else if (strcmp(cmd[0], "list\n") == 0){
        List();
    }
    else if (strcmp(cmd[0], "share") == 0){
        vector<char *> path;
        parse_path(cmd, path);
        Share(path);
    }
    else if (strcmp(cmd[0], "download") == 0){
        Download(cmd);
    }
  }
  ni_thread.join();
}

void parse(vector<char *> &cmd, char *input){
  char * token;
  token = strtok(input, " ");
  cmd.push_back(token);
  while(token != NULL){
    token = strtok(NULL, " ");
    cmd.push_back(token);
  }
}

void parse_path(vector<char *> &cmd, vector<char *> &path){
    char * token;
    if (cmd[1][strlen(cmd[1]) - 1] == '\n'){
        cmd[1][strlen(cmd[1]) - 1] = '\0';
    }
    token = strtok(cmd[1], "/");
    if (token == NULL){
        path.push_back(cmd[1]);
        return;
    }
    path.push_back(token);
    while(token != NULL){
        token = strtok(NULL, "/");
        path.push_back(token);
    }
}

void Register(){
  int sockfd, n, msg, len;
  char recvline[MAXLINE + 1];
  struct sockaddr_in  servaddr;

  printf("Preparing to connect to server. Type \"shutdown\" to disconnect.\n");

  if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
        exit( 2 );
  }
  // build a profile for the server to whom we are going to
  // communicate.  
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port   = htons(SERVER_PORT);
  
  if (inet_pton(AF_INET, IP, &servaddr.sin_addr) <= 0) {
      fprintf( stderr, "inet_pton error for %s\n", IP );
      return;
  }
  // Attempt to connect to the server.
  if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
      fprintf( stderr, "connect error: %s\n", strerror( errno ) );
      return;
  }
  printf("*** Successful connection to server. ***\n");

  char cmd[] = "sign";

  if ((write(sockfd,&cmd,sizeof(cmd))) < 0) {
      printf("client: write error when sending \"sign\" command:%s\n",  strerror( errno )); 
      return;
  }
  printf("*** Successful \"sign\" command sent to server ***.\n");

  if ((len = read(sockfd, recvline, MAXLINE)) < 0){
      fprintf(stderr, "client: read error after sending \"sign\" command :%s\n", strerror(errno));
      return;
  }
  recvline[len] = '\0';

  if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error after sending \"sign\" command : %s\n", strerror( errno ) );
      return;
  }
  cout << "*** Server successfully grabbed client's IP ***" << endl;

  
  if ((write(sockfd,MYPORT,sizeof(MYPORT))) < 0) {
      printf("client: write error when writing port number :%s\n",  strerror( errno )); 
      return;
  }
  cout << "*** Client successfully wrote its port number to server ***" << endl;

  if ((len = read(sockfd, recvline, MAXLINE)) < 0){
      fprintf(stderr, "client: read error after sending port number :%s\n", strerror(errno));
      return;
  }
  recvline[len] = '\0';
  
  if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error after sending port number: %s\n", strerror( errno ) );
      return;
  }
  cout << "*** Server successfully read client's port number ***" << endl;

  close( sockfd );
}

void List(){
    int sockfd, n, msg, len;
    char recvline[MAXLINE + 1];
    struct sockaddr_in  servaddr, cliaddr;

    printf("Preparing to connect to server.\n");

    if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
        exit( 2 );
    }
    // build a profile for the server to whom we are going to
    // communicate.  
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port   = htons(SERVER_PORT);

    if (inet_pton(AF_INET, IP, &servaddr.sin_addr) <= 0) {
      fprintf( stderr, "inet_pton error for %s\n", IP );
      return;
    }
    // Attempt to connect to the server.
    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
      fprintf( stderr, "connect error: %s\n", strerror( errno ) );
      return;
    }
    printf("*** Successful connection to server. ***\n");
    
    cout << "*** About to receive list of clients and cache-servers ***" << endl;
    char cmd[] = "list";
    if ((write(sockfd,&cmd,sizeof(cmd))) < 0) {
      printf("client: write error when writing \"list\" command :%s\n",  strerror( errno )); 
      return;
    }
    cout << "*** Client successfully wrote \"list\" command to server ***" << endl;

    int num_clients;
    cout << "Accepting number of clients and cache-servers" << endl;
    if ((len = read(sockfd, &num_clients, sizeof(num_clients))) < 0){
      fprintf(stderr, "client: read error when reading number of clients and cache-servers :%s\n", strerror(errno));
      return;
    }
    if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error when reading number of clients and cache-servers : %s\n", strerror( errno ) );
      return;
    }
    cout << "*** Received number of clients and cache-servers ***" << endl;

    char cont[] = "Accepted";   
    if( write(sockfd, cont, strlen(cont)) < 0 ) {
      fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
      return;
    }
    cout << "*** Ready to accept client data ***" << endl;

    vector<Client> registered;
    for (int i = 0; i < num_clients; i++){
        Client c;
        cout << "Accepting a client's IP" << endl;
        if ((len = read(sockfd, &c.IP, sizeof(c.IP))) < 0){
          fprintf(stderr, "client: read error when reading incoming IP :%s\n", strerror(errno));
          return;
        }
        if (len < 0) {
          printf("%i\n", n);
          fprintf( stderr, "read error when reading incoming IP : %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Received IP ***" << endl;

        if( write(sockfd, cont, strlen(cont)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Ready to accept client's port ***" << endl;
        cout << "Accepting a client's port" << endl;
        if ((len = read(sockfd, &c.Port, sizeof(c.Port))) < 0){
          fprintf(stderr, "client: read error when reading incoming port number :%s\n", strerror(errno));
          return;
        }
        if (len < 0) {
          printf("%i\n", n);
          fprintf( stderr, "read error when reading incoming port number : %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Received port number ***" << endl;

        if( write(sockfd, cont, strlen(cont)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Received client's data ***" << endl;
        registered.push_back(c); 
    }
    close( sockfd );
    
    for (int i = 0; i < registered.size(); i++){
        //connect directly to client
        if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
            exit( 2 );
        }
        // build a profile for the server to whom we are going to
        // communicate.  
        bzero(&cliaddr, sizeof(cliaddr));
        cliaddr.sin_family = AF_INET;
        unsigned short port = (unsigned short) strtoul(registered[i].Port, NULL, 0);
        cliaddr.sin_port   = htons(port);

        if (inet_pton(AF_INET, registered[i].IP, &cliaddr.sin_addr) <= 0) {
          fprintf( stderr, "inet_pton error for %s\n", registered[i].IP );
          return;
        }
        // Attempt to connect to the server.
        if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
          fprintf( stderr, "connect error: %s\n", strerror( errno ) );
          return;
        }
        printf("*** Successful connection to another client ***\n");
   
        char cmd[] = "cach";
        if ((write(sockfd,&cmd,sizeof(cmd))) < 0) {
          printf("client: write error when writing \"cach\" command :%s\n",  strerror( errno )); 
          return;
        }
        cout << "*** Client successfully wrote \"cach\" command to server ***" << endl;
 
        if ((len = read(sockfd, &recvline, sizeof(recvline))) < 0){
          fprintf(stderr, "client: read error when reading cache-server status :%s\n", strerror(errno));
          return;
        }
        recvline[len] = '\0';
        if (len < 0) {
          printf("%i\n", n);
          fprintf( stderr, "read error when reading cache-server status : %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Received cache-server status ***" << endl << endl;
        close(sockfd);

        if (strcmp(recvline, "yes") == 0){
            // client is cache-server
            cout << "*** This client is a cache-server ***" << endl;
            if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
                exit( 2 );
            }
      
            bzero(&cliaddr, sizeof(cliaddr));
            cliaddr.sin_family = AF_INET;
            unsigned short port = (unsigned short) strtoul(registered[i].Port, NULL, 0);
            cliaddr.sin_port   = htons(port);

            if (inet_pton(AF_INET, registered[i].IP, &cliaddr.sin_addr) <= 0) {
              fprintf( stderr, "inet_pton error for %s\n", registered[i].IP );
              return;
            }
            // Attempt to connect to the server.
            if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
              fprintf( stderr, "connect error: %s\n", strerror( errno ) );
              return;
            }
            printf("*** Successful connection to cache-server ***\n");

            cout << "*** About to receive list of cache-server's clients ***" << endl;
            char cmd[] = "dump";
            if ((write(sockfd,&cmd,sizeof(cmd))) < 0) {
              printf("client: write error when writing \"dump\" command :%s\n",  strerror( errno )); 
              return;
            }
            cout << "*** Client successfully wrote \"dump\" command to server ***" << endl;
            
            int clients_cached;
            if ((len = read(sockfd, &clients_cached, sizeof(clients_cached))) < 0){
              fprintf(stderr, "client: read error when reading number of clients cached :%s\n", strerror(errno));
              return;
            }
            if (len < 0) {
              printf("%i\n", n);
              fprintf( stderr, "read error when reading number of clients cached : %s\n", strerror( errno ) );
              return;
            }
            cout << "*** Received number of clients cached by this cache-server ***" << endl;

            if ((write(sockfd,&cont,sizeof(cont))) < 0) {
              printf("client: write error when writing continue message :%s\n",  strerror( errno )); 
              return;
            }
            cout << "*** Client successfully wrote continue message ***" << endl;

            for (int j = 0; j < clients_cached; j++){
                Client a_client;
                cout << "Accepting a client's IP" << endl;
                if ((len = read(sockfd, &a_client.IP, sizeof(a_client.IP))) < 0){
                  fprintf(stderr, "client: read error when reading incoming IP :%s\n", strerror(errno));
                  return;
                }
                if (len < 0) {
                  printf("%i\n", n);
                  fprintf( stderr, "read error when reading incoming IP : %s\n", strerror( errno ) );
                  return;
                }
                cout << "*** Received IP ***" << endl;

                if( write(sockfd, cont, strlen(cont)) < 0 ) {
                  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
                  return;
                }
                cout << "*** Ready to accept client's port ***" << endl;
                cout << "Accepting a client's port" << endl;
                if ((len = read(sockfd, &a_client.Port, sizeof(a_client.Port))) < 0){
                  fprintf(stderr, "client: read error when reading incoming port number :%s\n", strerror(errno));
                  return;
                }
                if (len < 0) {
                  printf("%i\n", n);
                  fprintf( stderr, "read error when reading incoming port number : %s\n", strerror( errno ) );
                  return;
                }
                cout << "*** Received port number ***" << endl;

                if( write(sockfd, cont, strlen(cont)) < 0 ) {
                  fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
                  return;
                }
                cout << "*** Received client's data ***" << endl;
                registered.push_back(a_client);
            }
            close(sockfd);
        }
    }

    cout << "*** Obtaining files shared by each client ***" << endl;
    for (int i = 0; i < registered.size(); i++){
        if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
            exit( 2 );
        }
        // build a profile for the server to whom we are going to
        // communicate.  
        bzero(&cliaddr, sizeof(cliaddr));
        cliaddr.sin_family = AF_INET;
        unsigned short port = (unsigned short) strtoul(registered[i].Port, NULL, 0);
        cliaddr.sin_port   = htons(port);

        if (inet_pton(AF_INET, registered[i].IP, &cliaddr.sin_addr) <= 0) {
          fprintf( stderr, "inet_pton error for %s\n", registered[i].IP );
          return;
        }
        // Attempt to connect to the server.
        if (connect(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr)) < 0) {
          fprintf( stderr, "connect error: %s\n", strerror( errno ) );
          return;
        }
        printf("*** Successful connection to client ***\n");
   
        char cmd[] = "list";
        if ((write(sockfd,&cmd,sizeof(cmd))) < 0) {
          printf("client: write error when writing \"list\" command to client :%s\n",  strerror( errno )); 
          return;
        }
        cout << "*** Client successfully wrote \"list\" command to client ***" << endl;
        
        int num_files;
        cout << "*** Reading number of files this client has shared ***" << endl;
        if ((len = read(sockfd, &num_files, sizeof(num_files))) < 0){
          fprintf(stderr, "client: read error when reading number of shared files :%s\n", strerror(errno));
          return;
        }
        if (len < 0) {
          printf("%i\n", n);
          fprintf( stderr, "read error when reading number of shared files : %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Received number of shared files ***" << endl;

        if ((write(sockfd,&cont,sizeof(cont))) < 0) {
          printf("client: write error when writing continue message to client :%s\n",  strerror( errno )); 
          return;
        }

        cout << "*** Starting to read shared file names ***" << endl;
        for (int j = 0; j < num_files; j++){
            File a_file;
            if ((len = read(sockfd, &a_file.file_name, sizeof(a_file.file_name))) < 0){
              fprintf(stderr, "client: read error when reading file name :%s\n", strerror(errno));
              return;
            }
            if (len < 0) {
              printf("%i\n", n);
              fprintf( stderr, "read error when reading file name : %s\n", strerror( errno ) );
              return;
            }

            if ((write(sockfd,&cont,sizeof(cont))) < 0) {
              printf("client: write error when writing continue message to client :%s\n",  strerror( errno )); 
              return;
            }
            cout << "*** Successfully read file name ***" << endl;
            registered[i].files_shared.push_back(a_file);
        }
    }

    cout << '\n' << "Final list of clients and the files they've shared:" << endl;
    for (int i = 0; i < registered.size(); i++){
        cout << '\t' << "Client" << '\t' << registered[i].IP << '\t' << registered[i].Port << endl;
        for (int j = 0; j < registered[i].files_shared.size(); j++){
            cout << '\t' << '\t' << registered[i].files_shared[j].file_name << endl;
        }
    }
}

void Share(vector<char *> &path){
    File a_file;
    strcpy(a_file.file_name, path[path.size()-2]);
    string strpath = "";
    for (int i = 0; i < path.size()-1; i++){
        if (i > 0 && i < path.size()-1) strpath.append("/");
        strpath.append(path[i]);
    }
    strcpy(a_file.file_path, strpath.c_str());
    files_shared.push_back(a_file);
}

void Download(vector<char *> &cmd){

}

void ls_cmd(vector<char *> &cmd){
    string new_input = "";
    if (cmd.size() > 2){
        if (cmd[1][strlen(cmd[1]) - 1] == '\n'){
          cmd[1][strlen(cmd[1]) - 1] = '\0';
        }
        new_input.append("cd ");
        new_input.append(cmd[1]);
        new_input.append(" && ls");
    }
    else{
        if (cmd[0][strlen(cmd[0]) - 1] == '\n'){
          cmd[0][strlen(cmd[0]) - 1] = '\0';
        }
        new_input.append("ls");
    }

    FILE *in;
    char buff[MAXLINE];
    if ((in = popen(new_input.c_str(), "r")) != NULL){
        while(fgets(buff, sizeof(buff), in) != NULL){
            cout << buff;
        }
        pclose(in);
    }
}

void Shutdown(){
  printf("*** Client shutting down ***\n");
  int sockfd, n, msg, len;
  char recvline[MAXLINE + 1];
  struct sockaddr_in  servaddr;

  if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
        return;
  }
  // build a profile for the server to whom we are going to
  // communicate.  
  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port   = htons(SERVER_PORT);
  
  if (inet_pton(AF_INET, IP, &servaddr.sin_addr) <= 0) {
      fprintf( stderr, "inet_pton error for %s\n", IP );
      return;
  }
  // Attempt to connect to the server.
  if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
      fprintf( stderr, "connect error: %s\n", strerror( errno ) );
      return;
  }
  printf("*** Successful connection to server. ***\n");

  char cmd[] = "gbye";

  if ((write(sockfd,cmd,sizeof(cmd))) < 0) {
      printf("client: write error when sending \"gbye\" command:%s\n",  strerror( errno )); 
      return;
  }
  printf("*** Successful \"gbye\" command sent to server ***.\n");

  if ((len = read(sockfd, recvline, MAXLINE)) < 0){
      fprintf(stderr, "client: read error after sending \"gbye\" command :%s\n", strerror(errno));
      return;
  }
  recvline[len] = '\0';

  if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error after sending \"gbye\" command : %s\n", strerror( errno ) );
      return;
  }

  if ((write(sockfd,MYPORT,sizeof(MYPORT))) < 0) {
      printf("client: write error when sending port :%s\n",  strerror( errno )); 
      return;
  }
  printf("*** Port successfully sent to server ***.\n");

  if ((len = read(sockfd, recvline, MAXLINE)) < 0){
      fprintf(stderr, "client: read error after sending \"gbye\" command :%s\n", strerror(errno));
      return;
  }
  recvline[len] = '\0';

  if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error after sending \"gbye\" command : %s\n", strerror( errno ) );
      return;
  }

  cout << "*** Server removing client ***" << endl;

  close( sockfd );
}

void list_cmd(int connfd){
    char cont[] = "Accepted";
    char buff[MAXLINE];

    int num_files = files_shared.size();
    cout << "*** Sending number of files this client has shared ***" << endl;
    if( write(connfd, &num_files, sizeof(num_files)) < 0 ) {
      fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
      return;
    }

    if ((read(connfd, &buff, sizeof(buff))) < 0){
      fprintf(stderr, "client: read error after reading number of clients to cache :%s\n", strerror(errno));
      return;
    }
    cout << "*** Starting to send files that client shares ***" << endl;
    
    for (int i = 0; i < files_shared.size(); i++){
        if( write(connfd, &files_shared[i].file_name, sizeof(files_shared[i].file_name)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }

        if ((read(connfd, &buff, sizeof(buff))) < 0){
          fprintf(stderr, "client: read error after sending file name :%s\n", strerror(errno));
          return;
        }
        cout << "*** File name sent successfully ***" << endl;
    }
    cout << "*** All file names sent successfully ***" << endl;
}

void cach_cmd(int connfd){
    cout << "*** Checking if this client is a cache-server ***" << endl;
    if (ISCACHE){
        char cont[] = "yes"; 
        if( write(connfd, cont, strlen(cont)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Client responded that it is a cache-server ***" << endl;
    }
    else{
        char cont[] = "no";
        if( write(connfd, cont, strlen(cont)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Client responded that it is not a cache-server ***" << endl;
    }

    char buff[MAXLINE];
    if (read(connfd, &buff, sizeof(buff)) < 0){
      fprintf(stderr, "Read failed. %s\n", strerror(errno));
      exit(1);
    }
    cout << "Cache-server status accepted" << endl;

}

void serv_cmd(int connfd){
    int len, n;
    cout << "*** Server requesting that this client be made a cache-server ***" << endl;
    ISCACHE = true;
    char cont[] = "Accepted";
   
    if( write(connfd, cont, strlen(cont)) < 0 ) {
      fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
      return;
    }
    cout << "*** Client made into a cache-server ***" << endl;

    int num_to_cache;
    cout << "Reading number of clients to cache" << endl;
    if ((len = read(connfd, &num_to_cache, sizeof(num_to_cache))) < 0){
      fprintf(stderr, "client: read error after reading number of clients to cache :%s\n", strerror(errno));
      return;
    }
    cout << num_to_cache << " received" << endl;

    if (len < 0) {
      printf("%i\n", n);
      fprintf( stderr, "read error after reading number of clients to cache : %s\n", strerror( errno ) );
      return;
    }

    if( write(connfd, cont, strlen(cont)) < 0 ) {
      fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
      return;
    }
    cout << "*** Client accepted number of clients to cache ***" << endl;

    for (int i = 0; i < num_to_cache; i++){
        Client c;
        if ((len = read(connfd, &c, sizeof(c))) < 0){
          fprintf(stderr, "client: read error after reading in a client :%s\n", strerror(errno));
          return;
        }
        if (len < 0) {
          printf("%i\n", n);
          fprintf( stderr, "read error after reading in a client : %s\n", strerror( errno ) );
          return;
        }
        cout << "client received" << endl;
        clients_if_cached.push_back(c);

        if( write(connfd, cont, strlen(cont)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Cache-server accepted a client to cache ***" << endl;

    }

}

void dump_cmd(int connfd){
    printf("*** \"dump\" command received ***\n");

    int num_cached = clients_if_cached.size();
    if( write(connfd, &num_cached, sizeof(num_cached)) < 0 ) {
      fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
      return;
    }
    cout << "Sent client number of clients this cache-server tracks" << endl;
    
    char buff[MAXLINE];
    if (read(connfd, &buff, sizeof(buff)) < 0){
      fprintf(stderr, "Read failed. %s\n", strerror(errno));
      return;
    }
    cout << "Preparing to send clients tracked by this cache-server" << endl;
    
    for (int i = 0; i < num_cached; i++){
        if( write(connfd, &clients_if_cached[i].IP, sizeof(clients_if_cached[i].IP)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Successfully wrote IP to client ***" << endl;
 
         if (read(connfd, &buff, sizeof(buff)) < 0){
          fprintf(stderr, "Read failed. %s\n", strerror(errno));
          return;
        }
        cout << "*** IP accepted by client ***" << endl;
        if( write(connfd, &clients_if_cached[i].Port, sizeof(clients_if_cached[i].Port)) < 0 ) {
          fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
          return;
        }
        cout << "*** Successfully wrote port number to client ***" << endl;
 
         if (read(connfd, &buff, sizeof(buff)) < 0){
          fprintf(stderr, "Read failed. %s\n", strerror(errno));
          return;
        }
        cout << "*** port number accepted by client ***" << endl;
    }
}

void shut_cmd(int connfd){
    cout << "*** SERVER SHUTTING DOWN ***" << endl << "*** Shutting down all clients ***" << endl;
    char cont[] = "Accepted";
    if( write(connfd, cont, strlen(cont)) < 0 ) {
      fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
      return;
    }
    printf("*** Shutting down this client ***\n");
    exit(0);
}

void n_interactive(){
    int listenfd, connfd;
    socklen_t   len;
    struct sockaddr_in  servaddr, cliaddr;
    char  buff[MAXLINE];
  
    if( ( listenfd = socket(AF_INET, SOCK_STREAM, 0) ) < 0 ) {
    fprintf( stderr, "socket failed.  %s\n", strerror( errno ) );
    exit( 1 );
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;             // Communicate using the Internet domain (AF_INET)
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   // Who should we accept connections from?         
    unsigned short port = (unsigned short) strtoul(MYPORT, NULL, 0);
    servaddr.sin_port        = htons(port);  // Which port should the client listen on?        

    if( bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
        fprintf( stderr, "Bind failed.  %s\n", strerror( errno ) );
        exit( 1 );
    }

    if( listen(listenfd, LISTENQ) < 0 ) {
        fprintf( stderr, "Listen failed.  %s\n", strerror( errno ) );
        exit( 1 );
    }

    for ( ; ; ) {
      len = sizeof(cliaddr);

        if( ( connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len) ) < 0 ) {
            fprintf( stderr, "Accept failed.  %s\n", strerror( errno ) );
            exit( 1 );
        }
                  
        printf("\nconnection from %s, port %d\n",
         inet_ntop(AF_INET, &cliaddr.sin_addr, buff, sizeof(buff)),
         ntohs(cliaddr.sin_port));

        if (read(connfd, buff, sizeof(buff)) < 0){
            fprintf(stderr, "Read failed. %s\n", strerror(errno));
            exit(1);
        }
        else if (strcmp(buff, "serv") == 0){
            thread serv_thread(serv_cmd, connfd);
            serv_thread.join();
        }
        else if (strcmp(buff, "dump") == 0){
            thread dump_thread(dump_cmd, connfd);
            dump_thread.join();
        }
        else if (strcmp(buff, "cach") == 0){
            thread cach_thread(cach_cmd, connfd);
            cach_thread.join();
        }
        else if (strcmp(buff, "list") == 0){
            thread list_thread(list_cmd, connfd);
            list_thread.join();
        }
        else if (strcmp(buff, "shut") == 0){
            thread shut_thread(shut_cmd, connfd);
            shut_thread.join();
        }
        
        close(connfd);
    }
    close(listenfd);
}