#include <sys/types.h>	
#include <sys/socket.h>	/* basic socket definitions */
#include <netinet/in.h>	/* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define PORT_NUMBER 8130
#define MAXLINE 200

int main(int argc, char **argv)
{
    int sockfd, n, msg, len;
	char recvline[MAXLINE + 1];
	struct sockaddr_in	servaddr;

	if (argc != 2) {
	    fprintf( stderr, "Usage: %s <IPaddress>\n", argv[0] );
	    exit( 1 );
	}

	printf("Preparing to connect to server. Type 'q' to disconnect.\n");
	for(;;) {

		if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		    fprintf( stderr, "Socket error.  %s\n", strerror( errno ) );
		    exit( 2 );
		}
		// build a profile for the server to whom we are going to
		// communicate.  
		bzero(&servaddr, sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_port   = htons(PORT_NUMBER);
		
		if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0) {
		    fprintf( stderr, "inet_pton error for %s\n", argv[1] );
		    exit( 3 );
		}
		// Attempt to connect to the server.
		if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
		    fprintf( stderr, "connect error: %s\n", strerror( errno ) );
		    exit( 4 );
		}
		printf("*** Successful connection to server. ***\n");
		// talk to the server. (Application-level protocol) 
	    /* send message to server */
		char input[MAXLINE];
		printf("$ ");
		fgets(input, MAXLINE, stdin);

		if (input[0] == 'q'){
			printf("*** Closing connection ***.\n");
			close(sockfd);
			exit(0);
		}

	    if ((write(sockfd,input,sizeof(input))) < 0) {
	        printf("client: write  error :%s\n",  strerror( errno )); 
	        exit(0);
	    }
	    printf("*** Successful write to server ***.\n");
	  	
	  	if ((len = read(sockfd, recvline, MAXLINE)) < 0){
	    	fprintf(stderr, "client: read error :%s\n", strerror(errno));
	    	exit(0);
	    }
	    recvline[len] = '\0';

		if (len < 0) {
			printf("%i\n", n);
		    fprintf( stderr, "read error: %s\n", strerror( errno ) );
		    exit( 6 );
		}
	    printf("Server response: %s", recvline);
		close( sockfd );
	}
}
