Kelly Morales
Project 3 Phase 2
12/04/2015

Use makefile to compile:
    "make server" compiles the server.
    "make client" compiles the client.
To run:
   In one window: ./serverP3.x
   In another window: ./clientP3.x <Server's IP Address>
   You will be prompted to enter this client's port number.

This project supports the following commands:

* Note: The '$' prompt shows up twice when the client is first connecting, and it disappears if the server has connected to the client or on the server side if a client has connected to the server. Press enter and it will appear, but you can still enter commands when it isn't there.

Server Interactions:

	list - lists all clients and any cache-servers and any cache-server's clients.

	cache - command should be entered as "cache IP port" where IP is the IP of the client you wish to make a cache-server and port is it's port number. You will then be prompted to enter the number of files you would like to add to this cache server. After the next instructions, simply enter the IP and port number of the client you wish to add in the form "IP port". This cache command only properly functions when allowing one client to be tracked by the server.

	shutdown - shuts down all clients and any cache-server's clients

Client Interactions:

	shutdown - shuts down this client and informs the server so it can be removed from its tracked clients. Issues arise if a cache-server shuts down.

	ls - lists the files in the current directory

	ls dir-name - lists the files in the specified directory where dir-name is the path to said directory

	status - Displays whether this client is a cache-server or not, and a list of files it has shared.

	list - The output starts with countless status messages, then it displays the clients that have registered with the server, specifying them by their IP and port number. If there is a cache-server, it is specified and the cache-server's clients are also listed. The files for each client are also listed. Again, only cache-servers with one client are properly listed.

	share file-name - adds file-name to the list of files that are listed as being shared by clients. If a user enters a path before the file name, both are saved, but only the file's name will be displayed when asked for later.
