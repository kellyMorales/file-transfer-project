#include <sys/types.h>	
#include <sys/socket.h>	
#include <arpa/inet.h>
#include <netinet/in.h>	
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define PORT_NUMBER 8130
#define MAXLINE 200
#define LISTENQ 5

int main(int argc, char **argv)
{
    int	listenfd, connfd;
    socklen_t   len;
    struct sockaddr_in  servaddr, cliaddr;
    char	buff[MAXLINE];

    if( ( listenfd = socket(AF_INET, SOCK_STREAM, 0) ) < 0 ) {
	fprintf( stderr, "socket failed.  %s\n", strerror( errno ) );
	exit( 1 );

    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;             // Communicate using the Internet domain (AF_INET)
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   // Who should we accept connections from?         
    servaddr.sin_port        = htons(PORT_NUMBER);  // Which port should the server listen on?        

    if( bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
	fprintf( stderr, "Bind failed.  %s\n", strerror( errno ) );
	exit( 1 );
    }

    if( listen(listenfd, LISTENQ) < 0 ) {
	fprintf( stderr, "Listen failed.  %s\n", strerror( errno ) );
	exit( 1 );
    }

    for ( ; ; ) {// infinite loop to accept then serve conncection
	len = sizeof(cliaddr);

	if( ( connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len) ) < 0 ) {
	    fprintf( stderr, "Accept failed.  %s\n", strerror( errno ) );
	    exit( 1 );
	}

	int pid;
	if((pid = fork()) == 0){

	  printf("connection from %s, port %d\n",
		 inet_ntop(AF_INET, &cliaddr.sin_addr, buff, sizeof(buff)),
		 ntohs(cliaddr.sin_port));

	  if (read(connfd, buff, sizeof(buff)) < 0){
	  	fprintf(stderr, "Read failed. %s\n", strerror(errno));
	  	exit(1);
	  }

	  char *upper = buff;
	  int i = 0;

	  while (upper[i]){
	  	upper[i] = toupper(buff[i]);
	  	i++;
	  }

	  if( write(connfd, upper, strlen(upper)) < 0 ) {
	    fprintf( stderr, "Write failed.  %s\n", strerror( errno ) );
	    exit( 1 );
	  }

	  close(listenfd);
	  close(connfd);
	  exit(0);
    }
	close(connfd);
	}
}
